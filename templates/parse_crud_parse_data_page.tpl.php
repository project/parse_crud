<?php 
// First value of the list
$options = $form['parse_crud_classes_list']['#options'];
array_values($options);
$current_class = array_shift($options);
?>
<div id="parse-crud" class="parse-crud parse-core-date">
  <div class="parse-crud-form parse-crud-form-classes">
    <?php if(isset($form)) : ?>
      <div class="parse-form-inner"><?php print drupal_render($form); ?></div>
    <?php endif; ?>
  </div>
  <div id="parse-crud-callback-wrapper" class="parse-crud-class-data parse-crud-table">
    <div class="parse-add-object">
      <a href="/parse/add/<?php print $current_class; ?>"><?php print t('Add a new parse object') ?></a>
    </div>
    <div class="parse-crud-data-table"><?php print parse_crud_get_class_objects_table($current_class); ?></div>
    <!--<p><?php //print t('Choose a class from the select list above.'); ?></p> -->
  </div>
</div>