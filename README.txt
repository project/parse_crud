Usage

This module integrates with Parse so you can do simple CRUD operations 
(create, read, update and delete) to your Parse app from a Drupal site.
This module does not save anything on Drupal (database or files) 
it communicates directly with parse through Parse PHP SDK, 
for more info about Parse PHP SDK please check the PHP guide from Parse.

Dependencies:

-Elements
-Date

Installation
-Download and enable the module from /admin/modules.
-Go to /admin/config/parse/parse_keys and enter your Parse account API keys.
-Go to /admin/config/parse/parse_classes and choose what Parse classes 
you want to do CRUD operations on.
-Go to /parse and from there you can access your Parse classes 
and objects and from there you can do CRUD operations on parse objects.
-Don't forget to set up the right permissions 
from /admin/people/permissions#module-parse_crud.

Please note that this module still needs some enhancements, 
so your contributions is appreciated.