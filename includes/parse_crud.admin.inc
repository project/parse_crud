<?php

/**
 * @file
 * Admin settings form and OAuth authentication integration
 */

require_once 'parse_crud.pages.inc';

/**
 * Admin settings form
 */
function parse_crud_admin_settings() {
  $form = array();

  $form['api_keys'] = array(
    '#type' => 'fieldset', 
    '#title' => t('API Keys'),
    '#description' => t('Copy paste the needed Parse keys here.'), 
    '#weight' => 0, 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );

  $form['api_keys']['parse_crud_app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('App ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_crud_app_id', ''),
  );

  $form['api_keys']['parse_crud_master_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Master Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_crud_master_key', ''),
  );

  $form['api_keys']['parse_crud_rest_key'] = array(
    '#type' => 'textfield',
    '#title' => t('REST Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_crud_rest_key', ''),
  );

  $form['api_keys']['parse_crud_url'] = array(
    '#type' => 'textfield',
    '#title' => t('API URL'),
    '#required' => TRUE,
    '#default_value' => variable_get('parse_crud_url', 'https://api.parse.com/1/'),
  );

  $form['#submit'][] = 'parse_crud_admin_settings_submit';

  return system_settings_form($form);
}

/**
 * Submit handler for the admin settings form.
 */
function parse_crud_admin_settings_submit($form, &$form_state) {
  drupal_set_message(t('You can now choose what classes you want to access from <a href="/admin/config/parse/parse_classes">Parse Core Classes</a>'), 'status');
}

/**
 * Classes settings.
 */
function parse_crud_classes_settings() {
  $form = array();
  $form['parse_classes'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Classes'),
    '#weight' => 0, 
    '#collapsible' => FALSE, 
    '#collapsed' => FALSE,
  );

  // Retrieve classes names.
  $options = parse_crud_list_classes();
  $form['parse_classes']['parse_crud_classes_list'] = array(
    '#type' => 'select', 
    '#title' => t('List of Classes'), 
    '#default_value' => variable_get('parse_crud_classes_list'), 
    '#options' => $options, 
    '#description' => t('Choose the classes that you want to access. If no classes appeared please make sure that you entered the right keys on <a href="/admin/config/parse/parse_keys">Parse API Keys</a>.'), 
    '#multiple' => TRUE, 
    '#size' => min(12,count($options)),
    '#weight' => 0,
  );

  return system_settings_form($form);
}
