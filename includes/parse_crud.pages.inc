<?php

/**
 * @file
 * Parse CRUD pages include functions
 */

require 'vendor/autoload.php';

use Parse\ParseClient;
use Parse\ParseObject;
use Parse\ParseQuery;
use Parse\ParseSchema;
use Parse\ParseException;
use Parse\ParseFile;
use Parse\ParseRelation;
use Parse\ParseGeoPoint;

/**
 * @todo find a proper way to call this, hook_init didn't work for some reason.
 */
ParseClient::initialize(variable_get('parse_crud_app_id'), variable_get('parse_crud_rest_key'), variable_get('parse_crud_master_key'));

/**
 * Callback function for parse-core.
 */
function parse_crud_core_data() {
  $form = drupal_get_form('parse_crud_core_data_form');
  return theme('parse_data_page', array('form' => $form));
}

/**
 * Form callback for core classes select.
 */
function parse_crud_core_data_form($form, &$form_state) {
  $form['parse_crud_classes_list'] = array(
    '#title' => t('List of Classes'),
    '#type' => 'select',
    '#options' => variable_get('parse_crud_classes_list'),
    /**
     * @todo should we do something here?!
     */
    //'#default_value' => $default,
    '#ajax' => array(
      'callback' => 'ajax_parse_crud_core_data_forms_callback',
      'wrapper' => 'parse-crud-callback-wrapper',
      'method' => 'replace',
      'effect' => 'fade',
    ),
  );

  return $form;
}

/**
 * Form builder; adding a new parse object.
 *
 * @ingroup forms
 * @see parse_crud_obj_form_validate()
 * @see parse_crud_obj_form_submit()
 */
function parse_crud_obj_form($form, &$form_state, $className = '', $objId = NULL) {
  $path = explode('/', current_path());
  if (isset($path[1]) && $path[1] == 'edit') {
    $objArray = parse_crud_retrieve_object_as_array($className, $objId);
  }
  $class = parse_crud_get_class($className);
  $form = array();
  if (isset($class)) {
    // Setting up table headers
    foreach ($class['fields'] as $fieldName => $typeArr) {
      $type = $typeArr['type'];
      // no need to show the objectId field as it's auto created from parse.com
      if ($fieldName != 'objectId' && $fieldName != 'createdAt' && $fieldName != 'updatedAt') {
        // our form should be dynamic based on the class coloumns types
        switch ($type) {
          case 'Number':
            $form['parse_crud_obj_' . $fieldName] = array(
              '#type' => 'numberfield', 
              '#title' => $fieldName,
              '#default_value' => (isset($objArray[$fieldName]) ? $objArray[$fieldName] : NULL),
              '#weight' => 2,
            );
            break;

          case 'String':
            $form['parse_crud_obj_' . $fieldName] = array(
              '#type' => 'textfield',
              '#title' => $fieldName,
              '#default_value' => (isset($objArray[$fieldName]) ? $objArray[$fieldName] : NULL),
              '#size' => 60, 
              '#maxlength' => 128,
              '#weight' => 1,
            );
            break;

          case 'Date':
            $date = isset($objArray[$fieldName]) ? $objArray[$fieldName]->format('Y-m-d') : NULL;
            $format = 'Y-m-d';
            $form['parse_crud_obj_' . $fieldName] = array(
              '#type' => 'date_popup',
              '#title' => preg_replace("/[\s_]/", " ", $fieldName),
              '#date_format' => $format,
              '#default_value' => $date,
              '#date_label_position' => 'within',
              '#weight' => 4,
            );
            break;

          case 'Boolean':
            $form['parse_crud_obj_' . $fieldName] = array(
              '#type' => 'radios',
              '#title' => $fieldName,
              '#options' => array(1 => t('True'), 0 => t('False')),
              '#default_value' => (isset($objArray[$fieldName]) ? $objArray[$fieldName] : NULL),
              '#weight' => 3,
            );
            break;

          case 'File':
            if (isset($objArray[$fieldName]) && !empty($objArray[$fieldName])) {
              $current_image = "<div class='parse-file-prefix'><a target='_blank' href='" . $objArray[$fieldName] . "'><img width='150px' src='" . $objArray[$fieldName] . "'/></a></div>";
            }
            $form['parse_crud_obj_' . $fieldName] = array(
              '#prefix' => (isset($current_image) ? $current_image : ''),
              '#title' => preg_replace("/[\s_]/", " ", $fieldName), 
              '#type' => 'file',
              '#title_display' => 'before',
              '#description' => t('Choose an image to be uploaded when saving the form.<br>Allowed extensions: jpg, jpeg, png, gif.'),
              '#weight' => 1,
            );
            break;

          case 'GeoPoint':
            if (isset($objArray[$fieldName])) {
              $latitude = $objArray[$fieldName]->getLatitude();
              $longitude = $objArray[$fieldName]->getLongitude();
            }
            $form[$fieldName . 'fieldset'] = array(
              '#type' => 'fieldset', 
              '#title' => $fieldName, 
              '#weight' => 5, 
              '#collapsible' => TRUE, 
              '#collapsed' => FALSE,
            );
            $form[$fieldName . 'fieldset']['parse_crud_obj_' . $fieldName . '_latitude'] = array(
                '#type' => 'textfield',
                '#title' => 'Latitude',
                '#default_value' => (isset($latitude) ? $latitude : NULL),
                '#size' => 60,
                '#maxlength' => 128,
              );
            $form[$fieldName . 'fieldset']['parse_crud_obj_' . $fieldName . '_longitude'] = array(
                '#type' => 'textfield',
                '#title' => 'Longitude',
                '#default_value' => (isset($longitude) ? $longitude : NULL),
                '#size' => 60,
                '#maxlength' => 128,
              );
            break;
          /**
           * @todo other type should be handled here, but for now this is what we need.
           */
          default:
            /** 
             * @todo Do we need to do something here?!
             */
            break;
        }
      }
    }
  }

  // we may need some intro here.
  //$form['intro'] = array('#markup' => $content);
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save'));

  return $form;
}

/**
 * parse_crud_obj_form_validate function.
 */
function parse_crud_obj_form_validate($form, &$form_state) {
  foreach ($form_state['complete form'] as $form_key => $form_element) {
    if (isset($form_element['#type']) && $form_element['#type'] == 'numberfield') {
      $form_state['values'][$form_key] = (int) $form_state['values'][$form_key];
    }
    if (isset($form_element['#type']) && $form_element['#type'] == 'file') {
      $file_field_name = $form_key;
      $file = file_save_upload($file_field_name, array(
        // Validates file is really an image.
        'file_validate_is_image' => array(),
        // Validate extensions.
        'file_validate_extensions' => array('png gif jpg jpeg'),
      ));
      // If the file passed validation:
      if ($file) {
        // Move the file into the Drupal file system.
        if ($file = file_move($file, 'public://')) {
          $parse_file = parse_crud_prepare_file($file);
          // Save the file for use in the submit handler.
          $form_state['values'][$file_field_name] = $parse_file;
        }
        else {
          form_set_error($file_field_name, t("Failed to write the uploaded file to the site's file folder."));
        }
      }
      else {
        $form_state['values'][$file_field_name] = NULL;
      }
    }
    if (isset($form_element['#type']) && $form_element['#type'] == 'radios') {
      $form_state['values'][$form_key] = (bool) $form_state['values'][$form_key];
    }
    if (isset($form_element['#type']) && $form_element['#type'] == 'date_popup') {
      $date = new DateTime($form_state['values'][$form_key]);
      $form_state['values'][$form_key] = $date;
    }
  }
  if ($form_state['values']['parse_crud_obj_Location_latitude'] && $form_state['values']['parse_crud_obj_Location_longitude']) {
    $point = new ParseGeoPoint((float) $form_state['values']['parse_crud_obj_Location_latitude'], (float) $form_state['values']['parse_crud_obj_Location_longitude']);
    $form_state['values']['parse_crud_obj_Location'] = $point;
    unset($form_state['values']['parse_crud_obj_Location_latitude']);
    unset($form_state['values']['parse_crud_obj_Location_longitude']);
  }
}

/**
 * parse_crud_obj_form_submit function
 */
function parse_crud_obj_form_submit($form, &$form_state) {
  $data = array();
  $path = explode('/', current_path());
  if (isset($path[1])) {
    if ($path[1] == 'edit') {
      $op = 'edit';
      $objId = (isset($path[3]) ? $path[3] : NULL);
    }
    else {
      $op = 'add';
    }
  }
  $className = $form_state['build_info']['args'][0];
  $values = $form_state['values'];
  unset($values['submit']);
  unset($values['form_build_id']);
  unset($values['form_token']);
  unset($values['form_id']);
  unset($values['op']);

  foreach ($values as $key => $value) {
    if (!empty($value)) {
      $parseField = str_replace('parse_crud_obj_', '', $key);
      $values[$parseField] = $value;
    }
    unset($values[$key]);
  }

  $data = $values;
  if ($op == 'add') {
    $objId = parse_crud_save_object($className, $data);
    if ($objId) {
      drupal_set_message(t('A new <strong>@className</strong> object was created with the ID "@objId"', array('@className' => $className, '@objId' => $objId)), 'status');
    }
    else {
      drupal_set_message(t('Something went wrong!'), 'error');
    }
  }
  elseif ($op == 'edit') {
    $updated_object = parse_crud_update_object($className, $objId, $data);
    if ($updated_object) {
      drupal_set_message(t('Object <strong>@objId</strong> of class "@className" was updated.', array('@className' => $className, '@objId' => $objId)), 'status');
    }
    else {
      drupal_set_message(t('Something went wrong!'), 'error');
    }
  }
  else {
    drupal_set_message(t('Operation could not been identified!'), 'error');
  }
}

/**
 * Ajax callback for parse_crud_core_data_form.
 */
function ajax_parse_crud_core_data_forms_callback($form, $form_state) {
  $chosen_class = $form_state['input']['parse_crud_classes_list'];
  $objectsTable = parse_crud_get_class_objects_table($chosen_class);
  $objAddLink = '<div class="parse-add-object">
                <a href="/parse/add/' . $chosen_class . '">' . t("Add a new parse object") . '</a>
                </div>';

  if ($objectsTable) {
    return "<div id='parse-crud-callback-wrapper'>" . $objAddLink . $objectsTable . "</div>";
  }
  else {
    return "<div id='parse-crud-callback-wrapper'><p>" . $objAddLink . t('No data found!') . "</p</div>";
  }
}

/**
 * Helper function that get all objects of the selected class in a rendered table.
 *
 * @param $chosen_class
 * class name as a string.
 *
 * @return
 * rendered table on success.
 * False on failure.
 */
function parse_crud_get_class_objects_table($chosen_class = '') {
  // initializing arrays 
  $table_header = array();
  $data = array();

  if (isset($chosen_class)) {
    $query = new ParseQuery($chosen_class);
    // Get class fields
    $class = parse_crud_get_class($chosen_class);
    if (isset($class)) {
      // Setting up table headers
      foreach ($class['fields'] as $fieldName => $type) {
        if ($type['type'] != 'Relation') {
          // Don't include relation in the table,
          // we are not intrested in relations, 
          // this module is just for simple CRUD operations
          $table_header[] = $fieldName;
        }
        // lets add pointers
        if ($type['type'] == 'Pointer') {
          $includeKey = $type['targetClass'];
          // Pointers require includeKey
          $query->includeKey($includeKey);
        }
      }
      // select the pasre columns
      $query->select($table_header);
      $results = $query->find();
      // for the edit and delete links.
      $table_header[] = 'links';
      foreach ($results as $row_key => $row) {
        // get each column value for each row (object)
        foreach ($class['fields'] as $fieldName => $type) {
          if ($type['type'] == 'File') {
            $file = $row->get($fieldName);
            if (isset($file)) {
              $data[$row->getObjectId()][$fieldName] = '<a target="_blank" href="' . $file->getURL() . '">' . t('View File') . '</a>';
            }
            else {
              $data[$row->getObjectId()][$fieldName] = t('No file attached');
            }
          }
          elseif ($type['type'] == 'Date') {
            if ($fieldName == 'createdAt') {
              $dateObj = $row->getCreatedAt();
              $data[$row->getObjectId()][$fieldName] = $dateObj->format('Y-m-d H:i:s');
            }
            elseif ($fieldName == 'updatedAt') {
              $dateObj = $row->getUpdatedAt();
              $data[$row->getObjectId()][$fieldName] = $dateObj->format('Y-m-d H:i:s');
            }
            else {
              $dateObj = $row->get($fieldName);
              $data[$row->getObjectId()][$fieldName] = (isset($dateObj) ? $dateObj->format('Y-m-d H:i:s') : '');
            }
          }
          elseif ($fieldName == 'objectId') {
            $rowObjId = $row->getObjectId();
            $data[$row->getObjectId()][$fieldName] = $rowObjId;
          }
          elseif ($type['type'] == 'Relation') {
            // do nothing, we are not intrested in relations, 
            // this module is just for simple CRUD operations
          }
          elseif ($type['type'] == 'GeoPoint') {
            if ($row->get($fieldName)) {
              $data[$row->getObjectId()][$fieldName] = $row->get($fieldName)->getLatitude() . ', ' . $row->get($fieldName)->getLongitude();
            }
            else {
              $data[$row->getObjectId()][$fieldName] = 'No GeoPoint found.';
            }
          }
          elseif ($type['type'] == 'Pointer') {
            //@todo figure out what data to pull out ..
            // right now just the object id which we can then refrence and load or pop
            // up a modal dialog with the info from that class
            $pointer = $row->get($fieldName);
            $data[$row->getObjectId()][$fieldName] = $row->getObjectId();
          }
          else {
            // default behaviour for any other column
            $data[$row->getObjectId()][$fieldName] = $row->get($fieldName);
          }
        }
        $data[$row->getObjectId()]['links'] = "<a href='/parse/edit/" . $chosen_class . "/" . $rowObjId . "'>" . t('Edit') . "</a><br><a href='/parse/delete/" . $chosen_class . "/" . $rowObjId . "'>" . t('Delete') . "</a>";
      }
    }
  }

  if (!empty($data)) {
    return theme('table', array('header' => $table_header, 'rows' => $data));
  }
  else {
    return FALSE;
  }
}

/**
 * Form builder; Delete a parse object.
 *
 * @ingroup forms
 * @see parse_crud_delete_obj_form_validate()
 * @see parse_crud_delete_obj_form_submit()
 */
function parse_crud_delete_obj_form($form, &$form_state, $className = '', $objId = NULL) {
  $form = array();
  $form_state['storage']['className'] = $className;
  $form_state['storage']['objId'] = $objId;
  $question = t('Are you sure you want to delete this Parse object?');
  $content = "<div class='parse-crud-delete-confirm'><p>".t('The object with the id <strong>@objId</strong> from class <strong>@className</strong> will be deleted and cannot be retrieved again.<br>Are you sure you want to delete it?', array('@objId' => $objId, '@className' => $className))."</p></div>";

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Delete'));

  return confirm_form($form, $question, '/parse', $content, t('Delete'), t('Cancel'));
}

/**
 * parse_crud_delete_obj_form_submit function
 */
function parse_crud_delete_obj_form_submit($form, &$form_state) {
  $className = $form_state['storage']['className'];
  $objId = $form_state['storage']['objId'];

  $result = parse_crud_destroy_object($className, $objId);
  if ($result) {
    drupal_set_message(t('Object <strong>@objId</strong> from class <strong>@className</strong> has been deleted.', array('@objId' => $objId, '@className' => $className)), 'status');
    drupal_goto('parse');
  }
  else {
    drupal_set_message(t('Object <strong>@objId</strong> from class <strong>@className</strong> can not be deleted.', array('@objId' => $objId, '@className' => $className)), 'error');
  }
}

/**
 * Helper function to get all Parse classes.
 *
 * @return
 * An associative array for each class name and fields. 
 */
function parse_crud_get_all_classes() {
  $schemas = new ParseSchema();
  $classes = $schemas->all();

  return $classes;
}

/**
 * Helper function to get list of all classes.
 *
 * @return
 * An array with the classes names. 
 */
function parse_crud_list_classes() {
  $classesNameList = array();

  $classes = parse_crud_get_all_classes();
  foreach ($classes as $key => $class) {
    $className = $class['className'];
    $classesNameList[$className] = $className;
  }

  return $classesNameList;
}

/**
 * Helper function to get a Parse class.
 *
 * @return
 * An associative array for the class and it's fields.
 * FALSE on failure.
 */
function parse_crud_get_class($className = '') {
  if (!empty($className)) {
    $schema = new ParseSchema($className);
    $class = $schema->get();
    // we don't need to show the ACL field.
    unset($class['fields']['ACL']);
    return $class;
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to save a parse object.
 *
 * @param $class_name
 * The class name as a string.
 *
 * @param $data
 * The Parse object data as an associative array.
 * ex: $data["column_name" => "value"];
 *
 * @return
 * Parse object ID on success.
 * FALSE on failure.
 */
function parse_crud_save_object($class_name = '', array $data = array()) {
  if (!empty($class_name) || !empty($data)) {
    $classObj = new ParseObject($class_name);

    // Setting object values
    foreach ($data as $column => $value) {
      $classObj->set($column, $value);
    }

    try {
      $classObj->save();
      return $classObj->getObjectId();
    } catch (ParseException $ex) {  
      // Execute any logic that should take place if the save fails.
      // error is a ParseException object with an error code and message.
      drupal_set_message(t($ex->getMessage()), 'error');
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to retrieve parse object by ID.
 *
 * @param $class_name
 * The class name as a string.
 *
 * @param $objId
 * The object ID from Parse.
 *
 * @return
 * The requested Parse object as a key => value array.
 * FALSE on failure.
 */
function parse_crud_retrieve_object($class_name = '', $objId = '') {
  if (!empty($class_name) && !empty($objId)) {
    $query = new ParseQuery($class_name);
    try {
      $classObject = $query->get($objId);
      // The object was retrieved successfully.
      return $classObject;
    } catch (ParseException $ex) {
      // The object was not retrieved successfully.
      // error is a ParseException with an error code and message.
      drupal_set_message(t($ex->getMessage()), 'error');
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to retrieve parse object by ID.
 *
 * @param $class_name
 * The class name as a string.
 *
 * @param $objId
 * The object ID from Parse.
 *
 * @return
 * The requested Parse object as a key => value array.
 * FALSE on failure.
 */
function parse_crud_retrieve_object_as_array($class_name = '', $objId = '') {
  if (!empty($class_name) && !empty($objId)) {
    $objArray = array();
    $query = new ParseQuery($class_name);
    try {
      $classObject = $query->get($objId);
      // The object was retrieved successfully.
      $class = parse_crud_get_class($class_name);
      foreach ($class['fields'] as $fieldName => $type) {
        if ($type['type'] == 'File') {
          $file = $classObject->get($fieldName);
          if (isset($file)) {
            $objArray[$fieldName] = $file->getURL();
          }
          else {
            $objArray[$fieldName] = array();
          }
        }
        elseif ($fieldName == 'createdAt') {
          $dateObj = $classObject->getCreatedAt();
          $objArray[$fieldName] = $dateObj->format('Y-m-d H:i:s');
        }
        elseif ($fieldName == 'updatedAt') {
          $dateObj = $classObject->getUpdatedAt();
          $objArray[$fieldName] = $dateObj->format('Y-m-d H:i:s');
        }
        elseif ($fieldName == 'objectId') {
          // do nothing
        }
        elseif ($type['type'] == 'Relation') {
          // do nothing, we are not intrested in relations,
          // this module is just for simple CRUD operations
        }
        else {
          // default behaviour for any other column
          $objArray[$fieldName] = $classObject->get($fieldName);
        }
      }
      if (!empty($objArray)) {
        return $objArray;
      }
      else {
        return FALSE;
      }
    } catch (ParseException $ex) {
      // The object was not retrieved successfully.
      // error is a ParseException with an error code and message.
      drupal_set_message(t($ex->getMessage()), 'error');
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to update parse object by ID.
 *
 * @param $class_name
 * The class name as a string.
 *
 * @param $objId
 * The object ID from Parse.
 *
 * @param $data
 * The Parse object data as an associative array.
 * ex $data["column_name" => "value"];
 *
 * @return
 * The updated Parse object.
 * FALSE on failure.
 */
function parse_crud_update_object($class_name = '', $objId = '', array $data = array()) {
  if (!empty($class_name) || !empty($objId) || !empty($data)) {
    $classObj = parse_crud_retrieve_object($class_name, $objId);
    // Updating object values
    foreach ($data as $column => $value) {
      $classObj->set($column, $value);
    }

    try {
      $classObj->save();
      return $classObj;
    } catch (ParseException $ex) {  
      // Execute any logic that should take place if the save fails.
      // error is a ParseException object with an error code and message.
      drupal_set_message(t($ex->getMessage()), 'error');
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to destroy parse object by ID.
 *
 * @param $objId
 * The object ID from Parse.
 *
 * @return
 * True on success.
 * FALSE on failure.
 */
function parse_crud_destroy_object($class_name = '', $objId = '') {
  if (!empty($class_name) || !empty($objId)) {
    $classObj = parse_crud_retrieve_object($class_name, $objId);
    /**
     * @todo exceptions may not work here
     */
    try {
      $classObj->destroy();
      return TRUE;
    } catch (ParseException $ex) {
      // Execute any logic that should take place if the destroy fails.
      // error is a ParseException object with an error code and message.
      drupal_set_message(t($ex->getMessage()), 'error');
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Helper function to prepare file before send it to parse
 *
 * @param $fileObj
 * The file object.
 *
 * @return
 * The parse file object.
 * FALSE on failure.
 */
function parse_crud_prepare_file($fileObj = NULL) {
  $file = ParseFile::createFromData(file_get_contents($fileObj->uri), $fileObj->filename);
  $file->save();

  return $file;
}
